#include <iostream>
#include <boost/filesystem.hpp>

using namespace std;

int main(){
	int c=0;
	boost::filesystem::recursive_directory_iterator i("/home/csenge/src");
	while(i != boost::filesystem::recursive_directory_iterator() ){
		if(i-> path().extension() == ".java" && boost::filesystem::is_regular_file(i->path())){
			c++;
		}
		i++;
	}
	cout << "Number of .java files in this directory: " <<  c <<endl;
	return 0;
}

