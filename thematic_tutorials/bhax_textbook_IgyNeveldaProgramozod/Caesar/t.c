#define MAX_TITKOS 4096
#define OLVASAS_BUFFER 256
#define KULCS_MERET 5
#define _GNU_SOURCE

#include <stdio.h>
#include <unistd.h>
#include <string.h>

double
atlagos_szohossz (const char *titkos, int titkos_meret)
{
  int sz = 1;
  for (int i = 0; i < titkos_meret; ++i)
    if (titkos[i] == ' ')
      ++sz;

  return (double) titkos_meret / sz;
}

int
tiszta_lehet (const char *titkos, int titkos_meret)
{
  // a tiszta szoveg valszeg tartalmazza a gyakori magyar szavakat
  // illetve az átlagos szóhossz vizsgálatával csökkentjük a
  // potenciális töréseket

  double szohossz = atlagos_szohossz (titkos, titkos_meret);

  return szohossz > 2.0 && szohossz < 9.0
    && strcasestr (titkos, "az") && strcasestr (titkos, "nem")
    && strcasestr (titkos, "hogy") ;

}

void
exor (const char kulcs[], int kulcs_meret, char titkos[], int titkos_meret)
{

  int kulcs_index = 0;

  for (int i = 0; i < titkos_meret; ++i)
    {

      titkos[i] = titkos[i] ^ kulcs[kulcs_index];
      kulcs_index = (kulcs_index + 1) % kulcs_meret;

    }

}

int
exor_tores (const char kulcs[], int kulcs_meret, char titkos[],
	    int titkos_meret)
{

  exor (kulcs, kulcs_meret, titkos, titkos_meret);

  return tiszta_lehet (titkos, titkos_meret);

}

int
main (void)
{

  char kulcs[KULCS_MERET];
  char titkos[MAX_TITKOS];
  char *p = titkos;
  int olvasott_bajtok;


  // titkos fajt berantasa
  while ((olvasott_bajtok =
	  read (0, (void *) p,
		(p - titkos + OLVASAS_BUFFER <
		 MAX_TITKOS) ? OLVASAS_BUFFER : titkos + MAX_TITKOS - p)))
    p += olvasott_bajtok;

  // maradek hely nullazasa a titkos bufferben  
  for (int i = 0; i < MAX_TITKOS - (p - titkos); ++i)
    titkos[p - titkos + i] = '\0';

int t[20] ={'a','b','c','d','e','f','g','h','i','j','1','2','3','4','5','6','7','8','9','0'};
  // osszes kulcs eloallitasa
#pragma omp paralell for private (kulcs)

  for (int ii = 0; ii <=19; ++ii) 
      for (int ji =0; ji <=19; ++ji)  
        for (int ki =0; ki <=19; ++ki) 
          for (int li = 0; li <=19;++li) 
            for (int mi = 0; mi <=19; ++mi) { 
              //for (int ni = 0; ni <= 9; ++ni) {
                //for (int oi = 0; oi <= 9; ++oi) {
                  //for (int pi = 0; pi <= 9; ++pi) {
	    //for (int ni = '0'; ni <= '9'; ++ni)
	      //for (int oi = '0'; oi <= '9'; ++oi)
		//for (int pi = '0'; pi <= '9'; ++pi)
		  
		    kulcs[0] = t[ii];
		    kulcs[1] = t[ji];
		    kulcs[2] = t[ki];
		    kulcs[3] = t[li];
		    kulcs[4] = t[mi];
		    //kulcs[5] = ni;
		    //kulcs[6] = oi;
		    //kulcs[7] = pi;

		    if (exor_tores (kulcs, KULCS_MERET, titkos, p - titkos))
		      printf
			("Kulcs: [%c%c%c%c%c]\nTiszta szoveg: [%s]\n",
			 ii, ji, ki, li, mi, titkos);

		    // ujra EXOR-ozunk, igy nem kell egy masodik buffer  
		    exor (kulcs, KULCS_MERET, titkos, p - titkos);
		  }

  return 0;
}
