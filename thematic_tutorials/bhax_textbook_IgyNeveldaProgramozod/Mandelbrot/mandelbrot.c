#include <stdio.h>
#include <sys/times.h>
#include <stdlib.h>
#include <png.h>
#include <libpng16/png.h>

#define MERET 600
#define ITER_HAT 32000

void mandel (int kepadat[MERET][MERET]) {
	clock_t delta = clock ();
	struct tms tmsbuf1, tmsbuf2;
    	times (&tmsbuf1);


    	float a = -2.0, b = 0.7, c = -1.35, d = 1.35;
    	int szelesseg = MERET, magassag = MERET, iteraciosHatar = ITER_HAT;

    	float dx = (b - a) / szelesseg;
    	float dy = (d - c) / magassag;
    	float reC, imC, reZ, imZ, ujreZ, ujimZ;

    	int iteracio = 0;

    	for (int j = 0; j < magassag; ++j)
    	{
 	for (int k = 0; k < szelesseg; ++k)
        {
            
            reC = a + k * dx;
            imC = d - j * dy;
            reZ = 0;
            imZ = 0;
            iteracio = 0;
	while (reZ * reZ + imZ * imZ < 4 && iteracio < iteraciosHatar)
            {
                ujreZ = reZ * reZ - imZ * imZ + reC;
                ujimZ = 2 * reZ * imZ + imC;
                reZ = ujreZ;
                imZ = ujimZ;

                ++iteracio;

            }

            kepadat[j][k] = iteracio;
        }
    }

    times (&tmsbuf2);
    printf("%ld\n", tmsbuf2.tms_utime - tmsbuf1.tms_utime
              + tmsbuf2.tms_stime - tmsbuf1.tms_stime);

    delta = clock () - delta;
    printf("%f sec\n", (float) delta / CLOCKS_PER_SEC);

}

int
main (int argc, char *argv[])
{

    if (argc != 2)
    {
        printf("Hasznalat: ./mandelpng fajlnev\n");
        return -1;
    }

	FILE *fp;
	fp = fopen (argv[1], "wb");
	if (!fp) return -1;
	
	png_structp png_ptr = png_create_write_struct (PNG_LIBPNG_VER_STRING, NULL, NULL,NULL);
	if (!png_ptr) return -1;

	png_infop info_ptr =png_create_info_struct(png_ptr);
	if( !info_ptr){ 
		png_destroy_write_struct(&png_ptr, (png_infopp) NULL);
		return -1; }

	if (setjmp(png_jmpbuf(png_ptr)))
	{
	png_destroy_write_struct(&png_ptr, &info_ptr);
	fclose(fp);
	return -1; }

	png_init_io(png_ptr, fp);

	png_set_IHDR(png_ptr, info_ptr, MERET, MERET, 8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

	png_text title_text;
	title_text.compression = PNG_TEXT_COMPRESSION_NONE;
	title_text.key = "Cim";
	title_text.text = "Mandelbrot halmaz";
	png_set_text(png_ptr, info_ptr, &title_text, 1);

	png_write_info(png_ptr, info_ptr);
	png_bytep row = (png_bytep) malloc(3 * MERET * sizeof(png_byte));
	
	int kepadat[MERET][MERET];
	
	mandel(kepadat);
	for (int j = 0; j < MERET; ++j){
		for (int k = 0; k < MERET; ++k){
			row[k*3] = (255 - (255 * kepadat[j][k]) / ITER_HAT);
			row[k*3+1] = (255 - (255 * kepadat[j][k]) / ITER_HAT);
			row[k*3+2] = (255 - (255 * kepadat[j][k]) / ITER_HAT);
			row[k*3+3] = (255 - (255 * kepadat[j][k]) / ITER_HAT);}
		png_write_row(png_ptr, row);}
	png_write_end(png_ptr, NULL);			
	

	printf("%s mentve\n" ,argv[1]);

}
