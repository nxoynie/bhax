package Receptionist;


/**
* @generated
*/
public class Receptionist extends Worker {
    
    /**
    * @generated
    */
    private Integer appointments;
    
    
    
    /**
    * @generated
    */
    public Integer getAppointments() {
        return this.appointments;
    }
    
    /**
    * @generated
    */
    public Integer setAppointments(Integer appointments) {
        this.appointments = appointments;
    }
    
    
}
