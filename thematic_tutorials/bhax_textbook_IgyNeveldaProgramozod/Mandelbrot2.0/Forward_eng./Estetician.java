package Estetician;


/**
* @generated
*/
public class Estetician extends Worker {
    
    /**
    * @generated
    */
    private String makeupChoices;
    
    
    
    /**
    * @generated
    */
    public String getMakeupChoices() {
        return this.makeupChoices;
    }
    
    /**
    * @generated
    */
    public String setMakeupChoices(String makeupChoices) {
        this.makeupChoices = makeupChoices;
    }
    
    
}
