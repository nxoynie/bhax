package Haircolourist;


/**
* @generated
*/
public class Haircolourist extends Worker {
    
    /**
    * @generated
    */
    private String hairColourChoices;
    
    
    
    /**
    * @generated
    */
    public String getHairColourChoices() {
        return this.hairColourChoices;
    }
    
    /**
    * @generated
    */
    public String setHairColourChoices(String hairColourChoices) {
        this.hairColourChoices = hairColourChoices;
    }
    
    
}
