package Worker;


/**
* @generated
*/
public class Worker extends People {
    
    /**
    * @generated
    */
    private Integer payment;
    
    /**
    * @generated
    */
    private String position;
    
    /**
    * @generated
    */
    private Integer roomNumber;
    
    
    
    /**
    * @generated
    */
    public Integer getPayment() {
        return this.payment;
    }
    
    /**
    * @generated
    */
    public Integer setPayment(Integer payment) {
        this.payment = payment;
    }
    
    /**
    * @generated
    */
    public String getPosition() {
        return this.position;
    }
    
    /**
    * @generated
    */
    public String setPosition(String position) {
        this.position = position;
    }
    
    /**
    * @generated
    */
    public Integer getRoomNumber() {
        return this.roomNumber;
    }
    
    /**
    * @generated
    */
    public Integer setRoomNumber(Integer roomNumber) {
        this.roomNumber = roomNumber;
    }
    
    
}
