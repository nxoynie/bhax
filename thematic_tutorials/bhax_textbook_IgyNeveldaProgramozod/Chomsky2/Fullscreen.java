import java.awt.*;
import javax.swing.JFrame;
import javax.swing.JLabel;


public class Fullscreen extends JFrame {
    public static void main(String[] args) {
        GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice graphicsDevice = graphicsEnvironment.getDefaultScreenDevice();
        JFrame frame = new JFrame("Fullscreen");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        graphicsDevice.setFullScreenWindow(frame);
        Color c = new Color(218,112,214);
        Color a = new Color(255,255,255);
        frame.setForeground(a);
        JLabel label = new JLabel("Hi!!");
        label.setFont(new Font("Courier",Font.BOLD,50));
        frame.getContentPane().setBackground(c);

        frame.add(label);
        frame.pack();
        frame.setVisible(true);

    }
}

