package AbstractMethod;

import AbstractMethod.AbstractFactory;

public class Application {
    private Dress dress;
    private Gloves gloves;
    private Veil veil;

    public Application(AbstractFactory factory){
        dress= factory.createDress();
        gloves = factory.createGloves();
        veil = factory.createVeil();
    }
    public void choose() {
        dress.choose();
        gloves.choose();
        veil.choose();
    }
}
