package AbstractMethod;

public interface Dress {
    void choose();
}
