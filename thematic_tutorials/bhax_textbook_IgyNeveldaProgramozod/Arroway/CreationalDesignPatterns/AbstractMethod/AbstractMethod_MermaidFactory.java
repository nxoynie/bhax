package AbstractMethod;

public class MermaidFactory implements AbstractFactory {
    @Override
    public Dress createDress() {
        return new MermaidStyleDress();
    }

    @Override
    public Gloves createGloves() {
        return new MermaidStyleGloves();
    }

    @Override
    public Veil createVeil() {
        return new MermaidStyleVeil();
    }
}
