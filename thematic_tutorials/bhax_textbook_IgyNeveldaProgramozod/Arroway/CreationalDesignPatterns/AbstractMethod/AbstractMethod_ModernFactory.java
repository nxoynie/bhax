package AbstractMethod;

public class ModernFactory implements AbstractFactory {
    @Override
    public Dress createDress() {
        return new ModernStyleDress();
    }

    @Override
    public Gloves createGloves() {
        return new ModernStyleGloves();
    }

    @Override
    public Veil createVeil() {
        return new ModernStyleVeil();
    }
}

