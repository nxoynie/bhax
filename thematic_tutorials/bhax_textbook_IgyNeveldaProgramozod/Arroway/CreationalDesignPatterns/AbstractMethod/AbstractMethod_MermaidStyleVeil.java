package AbstractMethod;

public class MermaidStyleVeil implements Veil {
    @Override
    public void choose() {
        System.out.println("You have created a Modern style Veil");
    }
}
