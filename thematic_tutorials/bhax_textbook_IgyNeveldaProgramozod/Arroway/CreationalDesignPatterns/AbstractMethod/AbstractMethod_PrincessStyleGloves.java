package AbstractMethod;

public class PrincessStyleGloves implements Gloves {
    @Override
    public void choose() {
        System.out.println("You have created Princess style Gloves");
    }

}
