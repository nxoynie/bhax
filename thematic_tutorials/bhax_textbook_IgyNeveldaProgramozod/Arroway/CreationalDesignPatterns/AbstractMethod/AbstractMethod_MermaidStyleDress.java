package AbstractMethod;

public class MermaidStyleDress implements Dress {
    @Override
    public void choose() {
        System.out.println("You have created a Mermaid style Dress.");
    }
}
