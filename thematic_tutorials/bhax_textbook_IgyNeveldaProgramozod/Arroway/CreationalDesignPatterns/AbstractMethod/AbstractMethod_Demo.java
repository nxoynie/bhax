package AbstractMethod;

import AbstractMethod.AbstractFactory;
import AbstractMethod.MermaidFactory;
import AbstractMethod.ModernFactory;
import AbstractMethod.PrincessFactory;

public class AbstractMethod_Demo {
    private  static Application configureApplication() {
        Application app;
        AbstractFactory factory;
        String styleName = System.getProperty("style.name").toLowerCase();
        if(styleName.contains("mermaid")){
            factory = new MermaidFactory();
            app = new Application(factory);

        }else if(styleName.contains("princess")){
            factory = new PrincessFactory();
            app = new Application(factory);

        }else{
            factory = new ModernFactory();
            app = new Application(factory);
        }
        return app;
    }

    public static void main(String[] args) {
        Application app = configureApplication();
        app.choose();
    }
}
