package AbstractMethod;

public class ModernStyleVeil implements Veil {
    @Override
    public void choose() {
        System.out.println("You have created a Modern style Veil");
    }
}
