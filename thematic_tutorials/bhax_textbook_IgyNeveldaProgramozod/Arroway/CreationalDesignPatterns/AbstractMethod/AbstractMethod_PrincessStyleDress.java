package AbstractMethod;

public class PrincessStyleDress implements Dress {
    @Override
    public void choose() {
        System.out.println("You have created Princess style Dress");
    }
}
