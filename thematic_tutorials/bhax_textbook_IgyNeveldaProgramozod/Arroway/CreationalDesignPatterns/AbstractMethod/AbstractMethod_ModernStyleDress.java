package AbstractMethod;

public class ModernStyleDress implements Dress {
    @Override
    public void choose() {
        System.out.println("You have created a Modern style Dress");

    }
}
