package AbstractMethod;

public interface Veil {
    void choose();
}
