package AbstractMethod;

import AbstractMethod.AbstractFactory;

public class PrincessFactory implements AbstractFactory {
    @Override
    public Dress createDress() {
        return new PrincessStyleDress();
    }

    @Override
    public Gloves createGloves() {
        return new PrincessStyleGloves();
    }

    @Override
    public Veil createVeil() {
        return new PrincessStyleVeil();
    }
}
