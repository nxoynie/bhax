package AbstractMethod;

public class MermaidStyleGloves implements Gloves {
    @Override
    public void choose() {
        System.out.println("You have created Mermaid style Gloves");
    }

}
