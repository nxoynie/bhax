package AbstractMethod;

public class PrincessStyleVeil implements Veil {
    @Override
    public void choose() {
        System.out.println("You have created a Princess style Veil");
    }
}
