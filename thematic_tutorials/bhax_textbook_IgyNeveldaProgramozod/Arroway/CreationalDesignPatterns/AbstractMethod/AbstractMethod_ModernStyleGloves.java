package AbstractMethod;

public class ModernStyleGloves implements Gloves {
    @Override
    public void choose() {
        System.out.println("You have created Modern style Gloves");
    }
}
