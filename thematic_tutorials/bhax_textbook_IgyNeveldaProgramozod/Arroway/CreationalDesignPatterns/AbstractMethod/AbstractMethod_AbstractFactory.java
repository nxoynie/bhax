package AbstractMethod;

public interface AbstractFactory {
    Dress createDress();
    Gloves createGloves();
    Veil createVeil();
}
