package BuilderMethod;

public interface Builder {

    void setAge(int age);

    void setEyeColour(Colour colour);

    void setLanguage(Language language);

    void setVoice(Voice voice);

    void setCookingSkills(CookingSkills cookingSkills);

    void setMathsSkills(MathsSkills mathsSkills);

    void setMaths(MathsSkills mathsSkills);
}
