package BuilderMethod;

public class Director {
    public void constructsEnglishRobot(Builder builder) {
        builder.setAge(21);
        builder.setEyeColour(Colour.GREEN);
        builder.setLanguage(Language.ENGLISH);
        builder.setVoice(new Voice(20, 42));
        builder.setCookingSkills(new CookingSkills());
        builder.setMathsSkills(new MathsSkills());
    }

    public void constructGermanRobot(Builder builder) {
        builder.setAge(15);
        builder.setEyeColour(Colour.BLUE);
        builder.setLanguage(Language.GERMAN);
        builder.setVoice(new Voice(90, 75));
        builder.setCookingSkills(new CookingSkills());
        builder.setMathsSkills(new MathsSkills());
    }

    public void constructHungarianRobot(Builder builder) {
        builder.setAge(18);
        builder.setEyeColour(Colour.BROWN);
        builder.setLanguage(Language.HUNGARIAN);
        builder.setVoice(new Voice(100, 40));
        builder.setCookingSkills(new CookingSkills());
        builder.setMathsSkills(new MathsSkills());

    }
}
