package BuilderMethod;

public class Manual {
    private final int age;
    private final Colour colour;
    private final Language language;
    private final Voice voice;
    private final CookingSkills cookingSkills;
    private final MathsSkills mathsSkills;

    public Manual(int age, Colour colour, Language language, Voice voice, CookingSkills cookingSkills,MathsSkills mathsSkills){
        this.age =age;
        this.colour = colour;
        this.language= language;
        this.voice = voice;
        this.cookingSkills = cookingSkills;
        this.mathsSkills = mathsSkills;

    }
    public String print() {
        String info = " ";
        info +="Age of the Robot" + age + "\n";
        info +="Eye colour of the Robot" + colour + "\n";
        info +="Language of the Robot" + language + "\n";
        info +="Voice depth and volume of the Robot" + voice.getDepth() + ";volume -" + voice.getVolume() +"\n";
        if(this.cookingSkills != null) {
            info += "Cooking Skills: Got them" + "\n";
        }else{
            info += "Cooking Skills: N/A" + "\n";

        }
        if(this.mathsSkills != null){
            info += "Maths Skills: Got them" + "\n";
        }else{
            info += "Maths Skills: N/A" + "\n";
        }
        return info;
    }
}

