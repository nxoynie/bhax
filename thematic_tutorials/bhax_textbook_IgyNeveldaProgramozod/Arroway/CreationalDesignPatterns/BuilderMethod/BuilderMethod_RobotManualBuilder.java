package BuilderMethod;

public class RobotManualBuilder implements Builder{
    private int age;
    private Colour colour;
    private Language language;
    private Voice voice;
    private CookingSkills cookingSkills;
    private MathsSkills mathsSkills;

    @Override
    public void setAge(int age) {
        this.age = age;

    }

    @Override
    public void setEyeColour(Colour colour) {
        this.colour = colour;
    }

    @Override
    public void setLanguage(Language language) {
        this.language =language;
    }

    @Override
    public void setVoice(Voice voice) {
        this.voice = voice;
    }

    @Override
    public void setCookingSkills(CookingSkills cookingSkills) {
        this.cookingSkills = cookingSkills;
    }

    @Override
    public void setMathsSkills(MathsSkills mathsSkills) {

    }

    @Override
    public void setMaths(MathsSkills mathsSkills) {
        this.mathsSkills =mathsSkills;
    }
    public Manual getResult() {
        return new Manual(age,colour,language,voice,cookingSkills,mathsSkills);
    }
}
