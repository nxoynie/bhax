package BuilderMethod;

public class Demo {
    public static void main(String[] args) {
        Director director = new Director();

        RobotBuilder builder = new RobotBuilder();
        director.constructsEnglishRobot(builder);

        Robot robot = builder.getResult();
        System.out.println("Robot built:\n" + robot.getLanguage());

        RobotManualBuilder manualBuilder = new RobotManualBuilder();

        director.constructsEnglishRobot(manualBuilder);
        Manual robotManual= manualBuilder.getResult();
        System.out.println("\n Robot manual built:\n"+ robotManual.print());
    }

}
