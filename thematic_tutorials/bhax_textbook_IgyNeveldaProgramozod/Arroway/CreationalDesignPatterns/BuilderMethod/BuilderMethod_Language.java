package BuilderMethod;

public enum Language {
    ENGLISH, HUNGARIAN, GERMAN
}
