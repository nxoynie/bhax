package BuilderMethod;

public class CookingSkills {
    private  String recipe;

    public CookingSkills(){
        this.recipe= " 2 kg potato, 500 ml sourmilk, 2 tbsp salt, cheese";
    }
    public  CookingSkills(String manualRecipe){
        this.recipe =manualRecipe;
    }
    public  String getRecipe(){
        return recipe;
    }
}
