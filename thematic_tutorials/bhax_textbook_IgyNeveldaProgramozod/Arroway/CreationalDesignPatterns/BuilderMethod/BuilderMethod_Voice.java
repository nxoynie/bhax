package BuilderMethod;

public class Voice {
    private double volume;
    private double depth;
    private boolean started;

    public Voice(double volume, double depth){
        this.volume = volume;
        this.depth =depth;
    }
    public void on() {
        started= true;
    }
    public void off(){
        started = false;
    }
    public boolean isStarted(){
        return started;
    }
    public void play(double volume){
        if(started){
            this.volume += volume;
        }else{
            System.err.println("Cannot play(), you must switch on the Voice");
        }
    }

    public double getDepth() {
        return depth;
    }
    public double getVolume(){
        return volume;
    }
}
