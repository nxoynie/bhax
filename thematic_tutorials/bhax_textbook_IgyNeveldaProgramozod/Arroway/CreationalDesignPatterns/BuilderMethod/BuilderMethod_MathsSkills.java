package BuilderMethod;

public class MathsSkills {
    private Robot robot;

    public void setRobot(Robot robot){
        this.robot = robot;
    }
    public void sayWattLevel() {
        System.out.println(("Watt level:"+ robot.getWatts()));
    }
    public void showStatus(){
        if(this.robot.getVoice().isStarted()) {
            System.out.println("The Robot is ON");
        }else{
            System.out.println("The Robot is OFF");
        }
    }
}

