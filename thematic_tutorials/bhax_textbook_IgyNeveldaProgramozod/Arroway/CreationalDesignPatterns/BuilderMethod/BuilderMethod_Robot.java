package BuilderMethod;

public class Robot {
    private final int age;
    private final Colour colour;
    private final Language language;
    private final Voice voice;
    private final CookingSkills cookingSkills;
    private final MathsSkills mathsSkills;
    private double watts = 0;

    public Robot(int age,Colour colour, Language language, Voice voice, CookingSkills cookingSkills, MathsSkills mathsSkills){
        this.age = age;
        this.colour =colour;
        this.language = language;
        this.voice = voice;
        this.cookingSkills = cookingSkills;
        this.mathsSkills = mathsSkills;
    }

    public int getAge() {
        return age;
    }

    public Colour getColour() {
        return colour;
    }

    public Language getLanguage() {
        return language;
    }

    public Voice getVoice() {
        return voice;
    }

    public CookingSkills getCookingSkills() {
        return cookingSkills;
    }

    public MathsSkills getMathsSkills() {
        return mathsSkills;
    }

    public double getWatts() {
        return watts;
    }
    public void setWatts(double watts){
        this.watts =watts;
    }
}

