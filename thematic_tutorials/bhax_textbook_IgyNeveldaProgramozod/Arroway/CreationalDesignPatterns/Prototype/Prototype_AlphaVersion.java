package Prototype;

public class AlphaVersion implements Prototype {
    private String name = "AlphaVersion";


    @Override
    public Prototype clone() {
        return new AlphaVersion();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void execute() {
        System.out.println(name + ":first version");
    }
}

