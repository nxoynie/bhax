package Prototype;

public class FinalProduct implements Prototype {
    private String name = "FinalProduct";

    @Override
    public Prototype clone() {
        return new FinalProduct();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void execute() {
        System.out.println(name + "the final product");
    }
}
