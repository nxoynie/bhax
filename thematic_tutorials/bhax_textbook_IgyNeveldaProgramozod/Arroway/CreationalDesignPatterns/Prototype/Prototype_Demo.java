package Prototype;

import java.util.ArrayList;
import java.util.List;

public class Demo {
    public static void main(String[] args) {
        if (args.length > 0) {
            initializePrototypes();
            List<Prototype>prototypes = new ArrayList<>();
            for (String prototypeName :args){
                Prototype prototype = PrototypeModule.createPrototype(prototypeName);
                if(prototype != null){
                    prototypes.add(prototype);
                }
            }
            for (Prototype p: prototypes){
                p.execute();
            }
        }else{
            System.out.println("Run again with correct arguments");
        }
    }
    public static void initializePrototypes(){
        PrototypeModule.addPrototype(new AlphaVersion());
        PrototypeModule.addPrototype(new BetaVersion());
        PrototypeModule.addPrototype(new FinalProduct());
    }
}
