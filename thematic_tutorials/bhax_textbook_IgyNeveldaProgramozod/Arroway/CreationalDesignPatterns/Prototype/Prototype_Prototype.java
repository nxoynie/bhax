package Prototype;

public interface Prototype {
    Prototype clone();
    String getName();
    void execute();
}

