package Prototype;

public class BetaVersion implements Prototype{
    private  String name = "BetaVersion";

    @Override
    public Prototype clone() {
        return new BetaVersion();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void execute() {
        System.out.println(name + "second version");
    }
}
