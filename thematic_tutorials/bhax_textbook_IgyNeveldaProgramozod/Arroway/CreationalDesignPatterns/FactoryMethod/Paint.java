package FactoryMethod;

public interface Paint {
    void pack();
}
