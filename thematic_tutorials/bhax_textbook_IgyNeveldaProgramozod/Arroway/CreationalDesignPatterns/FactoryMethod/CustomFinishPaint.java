package FactoryMethod;

public class CustomFinishPaint implements Paint {
    @Override
    public void pack() {
        System.out.println("Custom finish mixing is needed");
    }
}
