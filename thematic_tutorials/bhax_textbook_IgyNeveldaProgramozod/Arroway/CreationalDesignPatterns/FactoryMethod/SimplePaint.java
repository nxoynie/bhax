package FactoryMethod;

public class SimplePaint implements Paint {
    @Override
    public void pack() {
        System.out.println("Leave the paint alone.");
    }

}
