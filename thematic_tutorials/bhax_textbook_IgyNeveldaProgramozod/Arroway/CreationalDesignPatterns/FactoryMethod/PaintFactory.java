package FactoryMethod;

public class PaintFactory {
    public static Paint getPaint(String CustomizationType){

        if (CustomizationType == null){
            return null;
        }
        if(CustomizationType.equalsIgnoreCase("CUSTOMCOLOR")){
            return new CustomColorPaint();
        }
        if(CustomizationType.equalsIgnoreCase("CUSTOMFINISH")){
            return new CustomFinishPaint();
        }
        if(CustomizationType.equalsIgnoreCase("SIMPLEPAINT")){
            return new SimplePaint();
        }
        return null;
    }
}
