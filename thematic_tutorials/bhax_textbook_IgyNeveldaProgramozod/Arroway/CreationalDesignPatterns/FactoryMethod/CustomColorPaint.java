package FactoryMethod;

public class CustomColorPaint implements Paint {
    @Override
    public void pack() {
        System.out.println("Custom color mixing is needed");
    }
}
