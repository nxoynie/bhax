package FactoryMethod;
import FactoryMethod.*;

public class FactoryMethod {
    public static void main(String[] args) {
        Paint paint1 = PaintFactory.getPaint("CUSTOMCOLOR");
        paint1.pack();

        Paint paint2 = PaintFactory.getPaint("CUSTOMFINISH");
        paint2.pack();

        Paint paint3 = PaintFactory.getPaint("SIMPLEPAINT");
        paint3.pack();

    }


}