package Singleton;

public class Demo {
    public static void main(String[] args) {
        System.out.println("Same value as used in singleton ==singleton was reused" + "\n"
        + "Different values == 2 singleton created" + "\n"
        +"RESULT" + "\n");
        Singleton singleton = Singleton.getInstance("FOO");
        Singleton anotherSingleton  = Singleton.getInstance("BAR");
        System.out.println(singleton.value);
        System.out.println(anotherSingleton.value);
    }
}
