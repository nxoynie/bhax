//TODO GNU v3
#include <iostream> // a kiiratás miatt szükséges
#include <random> // a randomfa építése miatt szükséges
#include <functional>
#include <chrono>

class Unirand { //random osztály

	private:
		std::function <int()> random; //függvény objektum ami int() és legyen random neve
	public: 
		Unirand(long seed, int min, int max): random( //legyen long , legyen egy minimum és egy maximum érték
			std::bind( //összekötöm a függvényt és a függvényargumentumát
			std::uniform_int_distribution <>(min, max),
			std::default_random_engine(seed)
			)
		)
		{}
	int operator()() {return random();} //operator() a neve, visszatérési értéke a random()

};

template < typename ValueType> //template osztály, és a típusa mindig változnak: valuetype
class BinRandTree{ //az ősanya 

protected: // azért tettünk ide adatokat ,mert így majd a gyerekosztály is örökölni fogja

	class Node { //beágyazott fa
	private: //privát tagok, a program más részéről nem átírhatóak
		ValueType value; //érték amit be kell csomagolni:valutype , value
		Node *left; //bal gyereke
		Node *right; //jobb gyereke
		int count{0}; //számoljuk a betű/szám/stb hányszor szerepel
		
		//TODO rule of five : ha egy osztályban van pointer akkor kell szerepelni destruktornak is
		Node(const Node &); //node másoló konstruktor , tiltom(privát rész) & <- bal értél referencia
		Node & operator=(const Node &); //másoló értékadás
		Node(const Node &&); //mozgató konstruktor
		Node & operator=(Node &&); //mozgató értékadás &&<- jobb érték referencia
		
	public: 
		Node(ValueType value,int count =0):  value(value),count(count),left(nullptr),right(nullptr){} //mivel templaten belül van el kell látni a típussal, a node objektumot építjük itt fel, baloldali és jobb oldali részfa pointere nullára állítva,
		ValueType getValue()const{return value;} //lekérdezi a value-t
		Node* leftChild()const{return left;} //lekérdezem a bal oldali részfát
		Node* rightChild()const{return right;} //lekérdezem a jobb oldali részfát
		void leftChild(Node* node){left =node;} // beállítha a bal oldali részfát
		void rightChild(Node* node){right=node;} //beállítja a jobb oldali részfát
		int getCount()const {return count;} //lekérdezem a számot
		void incCount(){++count;} //incrementálja a countot
	};
	
	Node *root; // a gyökér 
	Node *treep; // a fa pointer
	int depth{0}; // a fa mélység
private:

	//TODO rule of five
	
public:
	BinRandTree(Node *root = nullptr, Node *treep = nullptr): root(root), treep(treep){std::cout <<"BT ctor" <<std::endl; 
	} // a fa konstruktora, ahol a gyökér és fa pointer a default érték és nullára vannak állítva a root-ba tegye be a root paramétert és a treep-et a treepbe
	BinRandTree(const BinRandTree &old ){std::cout <<"BT copy ctor" <<std::endl;//masolo az std::cout-os részek csak a nyomkövetés a debugolás szempontjából kerültek a kódba
	
	root = cp(old.root, old.treep ); //átadom az old gyökerét és hogy hol tartottunk(treep)
	}
	Node* cp(Node *node, Node *treep){
		Node * newNode = nullptr; // ha null pointer akkor lemásolom simán a nullpointert nem megyek bele az if-be
		if(node)
		{
			newNode= new Node(node -> getValue(),node ->getCount());//lemásolom a régi node értékét és pointerét
		
		newNode -> leftChild (cp(node -> leftChild(), treep)); //a baloldlai részfa legyen a lemásolt baloldalirészfa
		newNode -> rightChild(cp(node -> rightChild(), treep)); //szimmetrikusan szintén
			if(node == treep) //ha a a lemásolandó egyenlő a fa pointer
				this -> treep = newNode; // akkor a fa pointer legyen az új node
		}
		return newNode; //visszaadom az új node-t
	}
	BinRandTree & operator=(const BinRandTree & old ){std::cout <<"BT copy assign" <<std::endl; //másoló értékadás
		BinRandTree tmp{old}; //az oldról készítek egy temporary másolatot
		std::swap(*this,tmp);//aktuális példány és másik cseréje
		return *this; //visszaadom az aktuális példányt	
	}
	
	BinRandTree(BinRandTree && old){std::cout <<"BT move ctor" <<std::endl; //mozgató konstruktor
		root = nullptr; // a gyökért egyenlővé teszem a nullpointerrel
		*this = std::move(old); //meghívja a másoló értékadást
	}
	BinRandTree & operator=(BinRandTree &&old){std::cout <<"BT move assign" <<std::endl; //mozgató értékadás
		std::swap(old.root, root); //kicserélem a régi gyökérét az új gyökerére
		std::swap(old.treep, treep); //cserélje le a régi fapointert az új fapointerre
		return *this; //adja vissza az aktuális példányt
	}
	
	~BinRandTree(){std::cout <<"BT dtor" <<std::endl; //a destuktor
	deltree(root); //törölje a gyökeret
	}
	BinRandTree & operator<<(ValueType value);
	void print(){print(root,std::cout);} //kiprinteli a fát
	void printr(){print(*root,std::cout);} 
	void print(Node *node, std::ostream & os); // a printelés megvalósítása
	void print(const Node &cnode, std::ostream & os);
	void deltree(Node *node);
	Unirand  ur {std::chrono::system_clock::now().time_since_epoch().count(),0,2};
	int whereToPut(){
	return ur();
	}
};
template <typename ValueType,ValueType vr,ValueType v0> //vr és v0 ,hogy a chart ne nullával jelezzük
class ZLWTree : public BinRandTree<ValueType> {
public:
	ZLWTree():BinRandTree<ValueType>(new typename BinRandTree<ValueType>::Node (vr)){ //eredetileg /-el jelültük a gyökeret de mostmár csinosítva van, a gyökeret adom át a Node-nak, támaszkodunk az ősozstályra és annak adjuk ét, mivel template osztályban vagyunk a fordító nem találja a Node-ot ezért meg kell neveznünk ,hogy hol található, és új típus névként megadni
	this -> treep = this -> root; //függetlenek a nevek a template osztálytól a paraméterek ,ezért a this-> -el minősítenünk kell ,hogy megtalálja a fordító
	}
	ZLWTree & operator<<(ValueType value); //ugyan úgy adom meg mint a másik fánál 

};

template <typename ValueType>
class BinSearchTree : public BinRandTree<ValueType> {
public:
	BinSearchTree(){}
	BinSearchTree & operator<<(ValueType value);

};
template <typename ValueType> //lemásoltuk a fa felépítését és kicsit átalakítottuk ,hogy random fát tudjunk építeni
BinRandTree<ValueType> & BinRandTree<ValueType>::operator<<(ValueType value){
	
	int rnd = whereToPut();
	
	if(!treep) {
		root = treep = new Node(value);} 
	else if (treep -> getValue() == value ) {
		treep -> incCount();} 
	else if (!rnd) { 
	treep = root;
	*this << value;
		}
	else if (rnd== 1) {
		if (!treep -> leftChild()) {
		treep-> leftChild (new Node(value));}
		else{
		treep = treep ->leftChild();
		*this << value;}
	} else if (rnd == 2) {
		if (!treep -> rightChild()) {
		treep-> rightChild (new Node(value));}
		else{
		treep = treep ->rightChild();
		*this << value;
	}
}
	treep = root;
	
	return *this;
}

template <typename ValueType>
BinSearchTree<ValueType> & BinSearchTree<ValueType>::operator<<(ValueType value){ //felépítjük a fát
	
	if(!this->treep) { //mutatja épp mit dolgozok fel, még nem létezik ezért tagadom
		this->root = this->treep = new typename BinRandTree<ValueType>::Node(value);} // létre hozok egy új Node-t ezért
	else if (this->treep -> getValue() == value ) { //ha a fa pointer egyenlő azzal amit be kell szúrni
		this->treep -> incCount();} //akkor incrementálom a countot 
	else if (this->treep -> getValue() > value ) { // ha a value kisebb mint az előtte levő
		if (!this->treep -> leftChild()) { //megnézem van e baldoldali gyereke
		this->treep-> leftChild (new typename BinRandTree<ValueType>::Node(value));} // ha nincs akkor csinálok
		else{ 
		this->treep = this->treep ->leftChild(); // ha van akkor legyen egyenlő a tree pointer a leftChilddal
		*this << value;}  // ugyanebben a fában meghívom a value-t 
	} else if (this->treep -> getValue() < value ) { 
		if (!this->treep -> rightChild()) { //szimmetrikusan ugyanúgy működik mint a baloldali részfánál
		this->treep-> rightChild (new typename BinRandTree<ValueType>::Node(value));}
		else{
		this->treep = this->treep ->rightChild();
		*this << value;
	}
}
	this->treep = this->root; // a tree pointert egyenlővé teszem a gyökérrel
	
	return *this; //vissza referenciálok az aktuális fára
}
template <typename ValueType,ValueType vr,ValueType v0> //vr és v0 ,hogy a chart ne nullával jelöljem
ZLWTree<ValueType,vr,v0> & ZLWTree<ValueType,vr,v0>::operator<<(ValueType value)
{ //ZLW-fa felépítése
	
	if(value == v0){ //ha az érték egyenlő nullával
		if(!this->treep ->leftChild()){ //ha nincs nullás gyereke
			typename BinRandTree<ValueType>::Node * node = new typename BinRandTree<ValueType>::Node(value); //akkor csinálok egy Node-ot számára 
			this->treep ->leftChild(node); //a tree pointert láncolom és a balgyerekbe beteszem a most elkészített node-ot
			this->treep = this-> root; //új dolgot behoztam szóval visszaugrok a gyökérbe
		}else {
		this->treep = this ->treep->leftChild(); // ha van gyereke akkor csak lentebb lépek
		}
	}else{ //jobboldali részfánál szimmetrikusan ugyanúgy működik mint a baloldali részfánál
		if(!this->treep ->rightChild()){
			typename BinRandTree<ValueType>::Node * node = new typename BinRandTree<ValueType>::Node(value);
			this->treep ->rightChild(node);
			this->treep = this-> root;
		}else {
		this->treep = this ->treep->rightChild();
		
		}
	}
	
	
	return *this; //láncolás miatt itt is visszadom az aktuális fát
}
template <typename ValueType> //a printelés template osztályon kívüli implementálása
	void BinRandTree<ValueType>::print(Node *node, std::ostream & os){
		if(node) // ha nem nullpointer
		{
		++depth; //amikor megyünk mélyebbre akkor növeljük a mélységet
		print(node ->leftChild(), os); //kiiratjuk a balgyereket az osra
		for(int i{0}; i <depth; ++i) //amíg az i kisebb mint a mélység kiírja és szintenként ---t shiftelünk 
			os << "---";
		 os << node->getValue() << " " << depth << " " << node->getCount() << std::endl; // kiírom a node értékét a mélységet és azt ,hogy hányszor volt
		print(node -> rightChild(), os); //kiiratjuk a jobbgyereket
		--depth; //amikor kijövünk csökkenti a mélységet
		}
	}
	template <typename ValueType>
	void BinRandTree<ValueType>::print(const Node &node, std::ostream & os){
		
		++depth;
		if(node.leftChild())
		print(*node.leftChild(), os);
		for(int i{0}; i <depth; ++i)
			os << "---";
		 os << node.getValue() << " " << depth << " " << node.getCount() << std::endl;
		
		if(node.rightChild())
		print(*node.rightChild(), os);
		--depth;
		}
	
	
template <typename ValueType>
	void BinRandTree<ValueType>::deltree(Node *node){ // hogy ne folyjon a memória ezért törlöm a fát
		if(node)
		{
		deltree(node -> leftChild()); //törlöm a baloldali részfát
		deltree(node -> rightChild()); //törlöm a jobb oldali részfát
		delete node; // ha a részfáakt töröltem törlöm magamat is tehát ez egy posztorder eljárás
		}
	}
	
BinRandTree<int> bar(){
	BinRandTree<int> bt;
	BinRandTree<int> bt2;
	Unirand r(0,0,1); 
	bt << 0 << 0 << 0;
	bt2 << 1 << 1 << 1;
	bt.print();
	std::cout <<"---"<< std::endl;
	bt2.print();
	return r()?bt:bt2;;
}	
	
	
BinRandTree<int> foo(){
	return BinRandTree<int>();
}
int main(int argc, char** argv, char ** env){ // env a környezeti változók
	BinRandTree<int> bt;// létrehozzuk a fát, template osztály miatt megadjuk miből fog állni a fa, ezesetben számokból
	bt <<1<<1; // így építem fel a fát, a bt objektumba shift-elem bele ezeket a számokat
	bt.print();
	std::cout <<"***"<< std::endl;
	BinRandTree<int> bt2{bar()}; //mozgató konstruktort várunk
	std::cout <<"***"<< std::endl;
	bt2.print();
	
	/*
	std::cout << std::endl;
	ZLWTree<char,'/','0'> zt; //ZLW fa ami a karakterekből áll 
	zt <<'0'<<'1'<<'0'<<'0'<<'1'<<'1'; // beshiftelem a karaktereket
	zt.print();
	ZLWTree<char,'/','0'>zt2{zt};//lemásolja ztből
	ZLWTree<char,'/','0'> zt3;
	zt3 <<'1'<<'1'<<'1'<<'1'<<'1';
	std::cout << "***" <<std::endl;
	zt=zt3; //másoló értékadás zt-t zt3-ba
	std::cout << "***" <<std::endl;
	ZLWTree<char,'/','0'> zt4 = std::move(zt2); //mozgató konstruktor
	*/
	}
	
	
	
	
	
