#include <iostream>
#include <functional>
#include <random>

class Unirand {

	private:
		std::function <int()> random;
	public: 
		Unirand(long seed, int min, int max): random(
			std::bind(
			std::uniform_int_distribution <>(min, max),
			std::default_random_engine(seed)
			)
		)
		{}
	int operator()() {return random();}

};

int main() {
	Unirand ur (42,0,2);
	for(int i{0};i<5;++i)
	std::cout << ur() << std::endl;
}
