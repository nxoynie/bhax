#include <iostream>

int fgv(){ return 41;}

int main(){
	int n= 7; //n=bal, 7=jobb
	int &lvr{n};
	int &&rvr{8};
	
	std::cout <<lvr << " " << rvr << std::endl;
	lvr = rvr;
	std::cout <<lvr << " " << rvr << std::endl;
	
	int &&rvr2{fgv()};
	lvr = rvr2;
	std::cout <<lvr << " " << rvr2<< std::endl;
return 0;
}
