import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Egy lehetséges megoldás!
 */
public class Refactored {
    public void refactored() {
        Runnable runnable = () -> System.out.println("Runnable!");
        runnable.run();

        Calculator calculator = number ->  number * number;

        Integer result = calculator.calculate(3);
        System.out.println("Calculation result: " + result);

        List<Integer> inputNumbers = Arrays.asList(1, null, 3, null, 5);
        List<Integer> resultNumbers = inputNumbers
                .stream()
                .filter(number -> number != null)       
                .map(number -> calculator.calculate(number))     
                .collect(Collectors.toList());

        Consumer<Integer> method = (Integer integer) -> System.out.println(integer);
        System.out.println("Result numbers: ");
        resultNumbers.forEach(method);

        Formatter formatter =(List<Integer> numbers) -> {
            String sb = numbers.stream()
                        .map(number -> String.valueOf(number))
                        .collect(Collectors.joining());
                        return sb;   
        };
        System.out.println("Formatted numbers: " + formatter.format(resultNumbers));
    }

}

