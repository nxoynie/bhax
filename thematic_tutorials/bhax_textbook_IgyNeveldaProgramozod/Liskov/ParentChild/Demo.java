package ParentChild;



public class Demo {
    public static void main(String[] args) {
        Parent parent1 =new Parent();
        Parent parent2 = new Child();

        System.out.println("Method of Parent");
        parent1.talking();

        System.out.println("Method of Child through Parent reference");
        parent2.sing("Nope, not working");

    }
}
