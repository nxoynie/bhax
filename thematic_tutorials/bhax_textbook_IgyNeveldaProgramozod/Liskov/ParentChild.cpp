#include <iostream>
#include <string>
class Parent {
    public:
	 void talking() {
        std::cout << "Parent says: I am the parent.\n";
    }
};

class Child: public Parent {
    public: 
	void sing(std::string string){
        std::cout << string << "\n";
    }
};
class Demo {
	int main(){
        Parent* parent1 =new Parent();
        Parent* parent2 = new Child();

        std::cout << "Method of Parent\n";
        parent1->talking();

        std::cout <<"Method of Child through Parent reference\n";
        parent2->sing("Nope, not working"); 
	
	delete parent1;
	delete parent2;

    }
};
