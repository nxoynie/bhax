#include <iostream>

class Cat {
	};
class HairyCats: public Cat {
public:
	virtual void hair()= 0;
};
class NonHairyCats: public Cat {
};

class OrangeTabby: public HairyCats {
public:
	void hair() override {
		std::cout<< "OrangeTabby Cat has shinny and smooth hair. \n";
				}
			};
class Sphynx: public NonHairyCats {
};

static void hairCat(HairyCats& cat)
{
	cat.hair();
}

int main()
{
	OrangeTabby newOrangeTabby;
	Sphynx	newSphynx;

	hairCat(newOrangeTabby);
	//hairCat(newSphynx);

	return 0;
}
