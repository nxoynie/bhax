package Car;

public class Vehicle {

    public Vehicle() {
        System.out.println("Vehicle is being created");
    }

    void start() {
        System.out.println("Buckle up, we are starting");
    }

}