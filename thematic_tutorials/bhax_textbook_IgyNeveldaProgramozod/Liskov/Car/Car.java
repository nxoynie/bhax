package Car;

public class Car extends Vehicle {

    public Car() {
        System.out.println("Car is being created.");
    }

    @Override
    void start() {
        System.out.println("Welcome, 007 agent, we are starting");
    }

}
