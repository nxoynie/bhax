package Car;

public class Supercar extends Car {

    public Supercar() {
        System.out.println("Supercar is being created.");
    }

    @Override
    void start() {
        System.out.println("Welcome Optimus Prime, your supercar is starting.");
    }

}
